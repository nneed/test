<?php

class User
{
	const TABLE = 'users';

	public static function findRand()
	{
		$db = Db::instance();
		if(!count(User::existTable())){
			User::importCsv();
		}
		$res = $db->query(
			'SELECT * FROM ' . self::TABLE .' ORDER BY RAND() LIMIT 1',
			self::class
			);
		return $res[0];
	}

	public function updateStatus()
	{
		$this->status = (int)(!$this->status);
		$values = [':status' => $this->status];
		$sql = 'UPDATE ' . static::TABLE . '
			SET status=:status
			WHERE id='.$this->id;
			;
		$db = Db::instance();
		$db->execute($sql, $values);
	}

 	public static function existTable()
 	{
		$db = Db::instance();
		$res = $db->query(
			'SHOW TABLES LIKE \'' . self::TABLE .'\'',
			self::class
			);
		return $res;
 	}

	public static function importCsv()
	{
		$db = Db::instance();
		$csv = new Csv();
		$sql = 'CREATE TABLE IF NOT EXISTS ' . self::TABLE . ' (
			  id int(10) unsigned NOT NULL AUTO_INCREMENT,
			  name varchar(255) NOT NULL,
			  status tinyint(1) NOT NULL,
			  PRIMARY KEY (id)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0';
		$db->execute($sql);
		$res = $db->execute($csv->import());
		if($res){
			echo "Импорт выполнен успешно<br>";
		}else{
			echo "Импорт не выполнен<br>";
		}
		return ;
	}

}