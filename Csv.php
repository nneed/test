<?php

class Csv
{

	public function import(
		$table = 'users', 		
		$afields = array('name','status'), 		
		$filename = '/test.csv', 	 	
					
		$delim=';',  		
		$enclosed='"',  	
		$escaped='\\"', 	 	
		$lineend='\\n',   	
		$hasheader=TRUE)
		{  	
	if($hasheader) $ignore = "IGNORE 1 LINES ";
	else $ignore = "";
	$q_import = 
	"LOAD DATA INFILE '".
		$_SERVER['DOCUMENT_ROOT'].$filename."' INTO TABLE ".$table." character set cp1251 ".
	"FIELDS TERMINATED BY '".$delim."' ENCLOSED BY '".$enclosed."' ".
	"    ESCAPED BY '".$escaped."' ".
	"LINES TERMINATED BY '".$lineend."' ".
	$ignore.
	"(".implode(',', $afields).")"
	;
		return $q_import;
	}
	

}