<?php

class Db
{
	protected $dbh;

	protected static $instance;

	protected function __construct()
	{

		try {
		    $this->dbh = new \PDO('mysql:host=127.0.0.1;dbname=test', 'root', 'paramatma'); // PDO Driver DSN. Throws A PDOException.
		}
		catch( Exception $e ) {
		   die('Ошибка подключения к базе данных');
		}
		
	}

	public static function instance()
	{
		if (null === self::$instance) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function execute($sql, $params = [] )
	{
		$sth = $this->dbh->prepare($sql);
		$res = $sth->execute($params);
		return $res;
	}
	public function query($sql, $class)
	{
		$sth = $this->dbh->prepare($sql);
		$res = $sth->execute();
		if (false !==$res) { 
			return $sth->fetchAll(\PDO::FETCH_CLASS, $class);
		}

		return [];
	}
}